# roll, or die trying

<b>Suboptimal</b>’s entry into the [GTMK Game Jam
2022](https://itch.io/jam/gmtk-jam-2022).

You can view/play/rate the entry here:
<https://cervid.itch.io/roll-or-die-trying>. Also see:
<https://subop.github.io/roll_of_the_dice/>.

The exact state of the game, as we submitted it to [itch\.io](https://itch.io/)
and the GMTK Game Jam 2022, is preserved in the `gmtk-jam-2022-submission`
branch (`git checkout gmtk-jam-2022-submission`). The code and assets submitted
to the game jam were all created during the game jam itself.

## authors

| author | email | responsibilities |
| :---- | :---- | :----- |
| deer | `capreolina`ⓐ`protonmail`◦`ch` | programming |
| Slime | `slimepleeze`ⓐ`gmail`◦`com` | visual art |
| Tab (“Outside”) | `braidgame`ⓐ`gmail`◦`com` | sound effects |
| Monc | | music |

## legal

All source code is licensed under the terms of the [GNU GPL version
3\.0](https://www.gnu.org/licenses/gpl-3.0.html), or any later version of the
same license, at your option.

All assets (images, audio, maps) are licensed under the terms of the [Creative
Commons Attribution-ShareAlike 4\.0
International](https://creativecommons.org/licenses/by-sa/4.0/) license (CC
BY-SA 4\.0).
