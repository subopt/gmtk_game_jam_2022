use bevy::ecs::component::Component;
use num_enum::{IntoPrimitive, TryFromPrimitive};

#[derive(
    Component,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    IntoPrimitive,
    TryFromPrimitive,
)]
#[repr(u8)]
pub enum DieFace {
    One = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
}

impl DieFace {
    /// In sorted order.
    pub fn adjacent_faces(self) -> [Self; 4] {
        match self {
            Self::One | Self::Six => {
                [Self::Two, Self::Three, Self::Four, Self::Five]
            }
            Self::Two | Self::Five => {
                [Self::One, Self::Three, Self::Four, Self::Six]
            }
            Self::Three | Self::Four => {
                [Self::One, Self::Two, Self::Five, Self::Six]
            }
        }
    }
}
