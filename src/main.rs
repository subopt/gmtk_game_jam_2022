#![forbid(unsafe_code)]

mod die;
mod map;
mod state;

use bevy::{
    prelude::*,
    window::{PresentMode, WindowMode},
};
use bevy_rapier2d::prelude::*;
use die::DieFace;
use map::Map;
use state::MapHalf;
use std::{cmp::Ordering, f32::consts::PI, num::NonZeroUsize};

const WINDOW_WIDTH: f32 = 1024.0;
const WINDOW_HEIGHT: f32 = 576.0;
const PIXELS_PER_METER: f32 = 1.0;
const PROJECTION_SCALE: f32 = 64.0;
const SPRITE_SCALE: f32 = 1.0 / 12.0;
const BACKDROP_SCALE: f32 = 1.0 / 8.0;
const SENSOR_SCALE: f32 = 15.0 / 16.0;
const INACTIVE_TILE_ALPHA: f32 = 7.0 / 8.0;
const GRAVITY_SCALE: f32 = 3.0 / 4.0;

#[derive(Component)]
struct Player;

#[derive(Component)]
struct PlayerSprite;

#[derive(Component)]
struct Anim {
    goal_frame: usize,
    timer: Timer,
}

#[derive(Component)]
struct AnimStartRot(f32);

#[derive(Component)]
struct Backdrop;

#[derive(Component)]
struct MapBoundary;

#[derive(Component)]
struct GroundCell;

#[derive(Component)]
struct GroundCellSprite;

struct TilesAtlas(Handle<TextureAtlas>);

struct MapStateChange {
    new_half: MapHalf,
    old_face: DieFace,
    new_face: DieFace,
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum PlayerTouchingFloor {
    No,
    Yes,
    CanDoubleJump,
}

struct Sfx {
    jump: Handle<AudioSource>,
    switch: Handle<AudioSource>,
    hit: Handle<AudioSource>,
    big_jump: Handle<AudioSource>,
}

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            width: WINDOW_WIDTH,
            height: WINDOW_HEIGHT,
            title: "roll, or die trying".to_owned(),
            present_mode: PresentMode::Fifo,
            resizable: false,
            cursor_visible: true,
            mode: WindowMode::Windowed,
            transparent: false,
            ..default()
        })
        .insert_resource(ClearColor(Color::rgb(0.627_5, 0.725_5, 0.737_3)))
        .add_plugins(DefaultPlugins)
        .add_plugin(RapierPhysicsPlugin::<()>::pixels_per_meter(
            PIXELS_PER_METER,
        ))
        .add_event::<MapStateChange>()
        .add_startup_system(setup)
        .add_system(process_collisions)
        .add_system(handle_input.after(process_collisions))
        .add_system(load_map)
        .add_system(move_camera)
        .add_system(update_tforms)
        .add_system(animate_sprites.after(update_tforms))
        .run();
}

fn setup(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
    mut atlases: ResMut<Assets<TextureAtlas>>,
    mut msc_er: EventWriter<MapStateChange>,
    audio: Res<Audio>,
) {
    // Set up camera.
    let mut camera_b = OrthographicCameraBundle::new_2d();
    camera_b.orthographic_projection.scale = 1.0 / PROJECTION_SCALE;
    commands.spawn_bundle(camera_b);

    // Load the map data.
    use map::GroundShape::*;
    let map = Map::new(
        NonZeroUsize::new(8).unwrap(),
        vec![FullR, FullR, FullR, FullR, FullR, FullR, FullR, FullR],
        vec![
            FullR, FullR, FullR, FullR, FullR, FullR, FullR, FullR,
            //
            FullL, FullL, FullL, MdR, FullR, FullR, FullR, FullR,
        ],
        Vec2::new(1.0, 2.0),
    ); //Map::from_json(include_str!("../assets/map0.json")).unwrap();
    let spawn_point = map.spawn_point();
    commands.insert_resource(map);

    // Load asset data...

    // Textures.
    let player_die_th = asset_server.load("player_die.png");
    let player_die_atlas =
        TextureAtlas::from_grid(player_die_th, Vec2::new(18.0, 18.0), 12, 30);
    let player_die_ah = atlases.add(player_die_atlas);

    let tiles_th = asset_server.load("tiles.png");
    let tiles_atlas =
        TextureAtlas::from_grid(tiles_th, Vec2::new(12.0, 12.0), 9, 4);
    let tiles_ah = atlases.add(tiles_atlas);
    commands.insert_resource(TilesAtlas(tiles_ah));

    // Audio.
    let jump_audio_src = asset_server.load("jump.ogg");
    let switch_audio_src = asset_server.load("switch.ogg");
    let hit_audio_src = asset_server.load("hit.ogg");
    let big_jump_audio_src = asset_server.load("big_jump.ogg");
    commands.insert_resource(Sfx {
        jump: jump_audio_src,
        switch: switch_audio_src,
        hit: hit_audio_src,
        big_jump: big_jump_audio_src,
    });
    audio.play_with_settings(
        asset_server.load("bgm.ogg"),
        PlaybackSettings {
            repeat: true,
            volume: 1.0,
            ..default()
        },
    );

    // Backdrop!
    for (i, path) in ["bg0.png", "bg1.png", "bg2.png", "bg3.png"]
        .into_iter()
        .enumerate()
    {
        commands
            .spawn()
            .insert(Backdrop)
            .insert_bundle(SpriteBundle {
                texture: asset_server.load(path),
                transform: Transform {
                    translation: Vec3::new(0.0, 0.0, (i as f32) / 3.0),
                    scale: Vec3::splat(BACKDROP_SCALE),
                    ..default()
                },
                ..default()
            });
    }

    // Spawn the player’s die.
    commands
        .spawn()
        .insert(Player)
        .insert(RigidBody::Dynamic)
        .insert(Collider::round_cuboid(0.25, 0.25, 0.25))
        .insert(Velocity::default())
        .insert(ExternalForce::default())
        .insert(ExternalImpulse::default())
        .insert(Damping {
            linear_damping: 1.0,
            angular_damping: 1.0,
        })
        .insert(Restitution {
            coefficient: 0.9,
            combine_rule: CoefficientCombineRule::Average,
        })
        .insert(Friction {
            coefficient: 1.0 / 3.0,
            combine_rule: CoefficientCombineRule::Average,
        })
        .insert(GravityScale(GRAVITY_SCALE))
        .insert(DieFace::One)
        .insert(AnimStartRot(0.0))
        .insert_bundle(TransformBundle::from(Transform {
            translation: spawn_point.extend(0.0),
            ..default()
        }));
    commands
        .spawn()
        .insert(PlayerSprite)
        .insert_bundle(SpriteSheetBundle {
            texture_atlas: player_die_ah,
            transform: Transform {
                translation: spawn_point.extend(100.0),
                scale: Vec3::splat(SPRITE_SCALE),
                ..default()
            },
            ..default()
        });
    commands.insert_resource(PlayerTouchingFloor::No);

    // Set the `MapHalf` state.
    commands.insert_resource(MapHalf::Fore);
    msc_er.send(MapStateChange {
        new_half: MapHalf::Fore,
        old_face: DieFace::Two,
        new_face: DieFace::One,
    });
}

fn load_map(
    mut msc_er: EventReader<MapStateChange>,
    mut commands: Commands,
    mut coll_q: Query<(Entity, &MapHalf, &mut Transform), With<Collider>>,
    mut tiles_q: Query<
        (&MapHalf, &mut Transform, &mut TextureAtlasSprite),
        (Without<Collider>, With<GroundCellSprite>),
    >,
    map: Res<Map>,
    tiles_atlas: Res<TilesAtlas>,
) {
    let mut state = None;
    for e in msc_er.iter() {
        state = Some(&e.new_half);
    }
    let state = if let Some(s) = state { s } else { return };

    let map_is_loaded = !coll_q.is_empty();

    // Swap colliders ↔ sensors.
    for (ent, mh, mut tform) in coll_q.iter_mut() {
        let mut em = commands.entity(ent);
        let is_new = mh == state;
        if is_new {
            em.remove::<Sensor>();
            tform.scale = Vec3::splat(1.0);
        } else {
            em.insert(Sensor);
            tform.scale = Vec3::splat(SENSOR_SCALE);
        }
    }

    // Adjust Z-indices and α values for visual purposes.
    for (mh, mut tform, mut sprite) in tiles_q.iter_mut() {
        let is_new = mh == state;

        let color = if is_new {
            Color::default()
        } else {
            let mut c = Color::default();
            c.set_a(INACTIVE_TILE_ALPHA);

            c
        };
        sprite.color = color;

        tform.translation.z = if is_new { 10.0 } else { 5.0 };
    }

    if map_is_loaded {
        return;
    }
    for (ground, mh) in [
        (map.foreground(), MapHalf::Fore),
        (map.background(), MapHalf::Back),
    ] {
        let is_new = mh == *state;

        // Map boundaries.
        let mut map_boundary_r = commands.spawn();
        map_boundary_r
            .insert(MapBoundary)
            .insert(mh)
            .insert(Collider::halfspace(-Vec2::X).unwrap())
            .insert_bundle(TransformBundle::from(Transform::from_xyz(
                ground.width().get() as f32,
                0.0,
                0.0,
            )));
        if !is_new {
            map_boundary_r.insert(Sensor);
        }
        let mut map_boundary_l = commands.spawn();
        map_boundary_l
            .insert(MapBoundary)
            .insert(mh)
            .insert(Collider::halfspace(Vec2::X).unwrap())
            .insert_bundle(TransformBundle::default());
        if !is_new {
            map_boundary_l.insert(Sensor);
        }
        let mut map_boundary_b = commands.spawn();
        map_boundary_b
            .insert(MapBoundary)
            .insert(mh)
            .insert(Collider::halfspace(Vec2::Y).unwrap())
            .insert_bundle(TransformBundle::default());
        if !is_new {
            map_boundary_b.insert(Sensor);
        }

        // Proper map colliders.
        for (shape, x, y) in ground.iter_coords() {
            if let Some(coll) = shape.collider() {
                let translation = Vec2::new(x as f32 + 0.5, y as f32 + 0.5);
                let mut map_coll = commands.spawn();
                map_coll
                    .insert(GroundCell)
                    .insert(mh)
                    .insert(coll)
                    .insert_bundle(TransformBundle::from(Transform {
                        translation: translation.extend(0.0),
                        rotation: Quat::IDENTITY,
                        scale: Vec3::splat(if is_new {
                            1.0
                        } else {
                            SENSOR_SCALE
                        }),
                    }));
                if !is_new {
                    map_coll.insert(Sensor);
                }

                let color = if is_new {
                    Color::default()
                } else {
                    let mut c = Color::default();
                    c.set_a(INACTIVE_TILE_ALPHA);

                    c
                };
                commands
                    .spawn()
                    .insert(GroundCellSprite)
                    .insert(mh)
                    .insert_bundle(SpriteSheetBundle {
                        sprite: TextureAtlasSprite {
                            index: usize::from(u8::from(shape))
                                + match mh {
                                    MapHalf::Fore => 0,
                                    MapHalf::Back => 18,
                                },
                            color,
                            ..default()
                        },
                        transform: Transform {
                            translation: translation.extend(if is_new {
                                10.0
                            } else {
                                5.0
                            }),
                            rotation: Quat::IDENTITY,
                            scale: Vec3::splat(SPRITE_SCALE),
                        },
                        texture_atlas: tiles_atlas.0.clone(),
                        ..default()
                    });
            }
        }
    }
}

fn update_tforms(
    player_q: Query<
        (&Transform, &DieFace, &AnimStartRot),
        (With<Player>, Without<PlayerSprite>),
    >,
    mut player_sprite_q: Query<
        (&mut Transform, &mut TextureAtlasSprite, Option<&Anim>),
        With<PlayerSprite>,
    >,
) {
    let (player_tform, &player_face, player_asr) = player_q.single();
    let (mut player_sprite_tform, mut player_atlas, maybe_anim) =
        player_sprite_q.single_mut();

    // Handle translation.
    let mut t = player_tform.translation;
    t.z = 100.0;
    t.y += 1.0 / 16.0; // Adjusting for the sprites being a bit off lol
    player_sprite_tform.translation = t;

    // Handle rotation.
    if maybe_anim.is_none() {
        player_sprite_tform.rotation = Quat::IDENTITY;

        let theta = 2.0 * PI - player_tform.rotation.to_euler(EulerRot::ZYX).0;
        let atlas_index = (theta * (12.0 / PI) + 0.5) as usize;
        let atlas_index = atlas_index % 12;

        player_atlas.index =
            12 * usize::from(u8::from(player_face) - 1) + atlas_index;
    } else {
        player_sprite_tform.rotation = Quat::from_euler(
            EulerRot::ZYX,
            player_tform.rotation.to_euler(EulerRot::ZYX).0 - player_asr.0,
            0.0,
            0.0,
        );
    }
}

fn animate_sprites(
    mut commands: Commands,
    time: Res<Time>,
    mut player_sprite_q: Query<
        (Entity, &mut TextureAtlasSprite, Option<&mut Anim>),
        With<PlayerSprite>,
    >,
    mut msc_er: EventReader<MapStateChange>,
) {
    let (player_sprite_ent, mut player_atlas, maybe_anim) =
        player_sprite_q.single_mut();

    if let Some(msc) = msc_er.iter().next() {
        let backwards = msc.new_half == MapHalf::Back;
        let (left_face, right_face) = if backwards {
            (msc.new_face, msc.old_face)
        } else {
            (msc.old_face, msc.new_face)
        };
        let chunk = usize::from(u8::from(left_face) - 1);
        let subchunk = left_face
            .adjacent_faces()
            .into_iter()
            .enumerate()
            .find(|(_, df)| *df == right_face)
            .unwrap()
            .0;
        let start_frame = 6 * 12
            + chunk * 4 * 12
            + subchunk * 12
            + if backwards { 5 - 1 } else { 0 };
        let goal_frame = if backwards {
            start_frame - (5 - 1)
        } else {
            start_frame + (5 - 1)
        };

        player_atlas.index = start_frame;
        commands.entity(player_sprite_ent).insert(Anim {
            goal_frame,
            timer: Timer::from_seconds(1.0 / 6.0, true),
        });
    } else if let Some(mut anim) = maybe_anim {
        anim.timer.tick(time.delta());
        if anim.timer.just_finished() {
            match player_atlas.index.cmp(&anim.goal_frame) {
                Ordering::Less => player_atlas.index += 1,
                Ordering::Greater => player_atlas.index -= 1,
                Ordering::Equal => {
                    commands.entity(player_sprite_ent).remove::<Anim>();
                }
            }
        }
    }
}

fn handle_input(
    keys: Res<Input<KeyCode>>,
    mut player_q: Query<
        (
            Entity,
            &mut ExternalForce,
            &mut ExternalImpulse,
            &Transform,
            &mut DieFace,
            &mut AnimStartRot,
            &Velocity,
            &mut GravityScale,
        ),
        With<Player>,
    >,
    mut ptf: ResMut<PlayerTouchingFloor>,
    mut map_state: ResMut<MapHalf>,
    mut msc_er: EventWriter<MapStateChange>,
    rapier_context: Res<RapierContext>,
    audio: Res<Audio>,
    sfx: Res<Sfx>,
) {
    let (
        player,
        mut ext_force,
        mut ext_imp,
        player_tform,
        mut player_face,
        mut player_asr,
        player_vel,
        mut player_grav,
    ) = player_q.single_mut();

    // Handle left and right movements.
    let weak_jump = *player_face == DieFace::Three;
    let strong_jump = *player_face == DieFace::Four;
    let force_mag = if weak_jump {
        30.0
    } else if strong_jump {
        12.0
    } else {
        20.0
    };
    if keys.pressed(KeyCode::Right) {
        *ext_force = ExternalForce {
            force: Vec2::new(force_mag, 0.0),
            torque: 0.0,
        };
    } else if keys.pressed(KeyCode::Left) {
        *ext_force = ExternalForce {
            force: Vec2::new(-force_mag, 0.0),
            torque: 0.0,
        };
    } else {
        *ext_force = default();
    }

    // Handle jumping.
    let double_jump_power = *player_face == DieFace::Two;
    let feather_fall_power = *player_face == DieFace::Five;
    let (can_jump, is_double_jumping, new_ptf) = match *ptf {
        PlayerTouchingFloor::No => (false, false, PlayerTouchingFloor::No),
        PlayerTouchingFloor::Yes => (
            true,
            false,
            if double_jump_power {
                PlayerTouchingFloor::CanDoubleJump
            } else {
                PlayerTouchingFloor::Yes
            },
        ),
        PlayerTouchingFloor::CanDoubleJump => {
            (double_jump_power, true, PlayerTouchingFloor::No)
        }
    };
    let attempting_to_jump = if is_double_jumping {
        keys.just_pressed(KeyCode::Space)
    } else {
        keys.pressed(KeyCode::Space)
    };
    if can_jump && attempting_to_jump {
        *ptf = new_ptf;

        *ext_imp = ExternalImpulse {
            impulse: Vec2::new(
                0.0,
                if is_double_jumping {
                    7.0
                } else if weak_jump {
                    3.0
                } else if strong_jump {
                    10.0
                } else {
                    5.0
                },
            ),
            torque_impulse: 0.0,
        };

        audio.play_with_settings(
            if strong_jump {
                sfx.big_jump.clone()
            } else {
                sfx.jump.clone()
            },
            PlaybackSettings {
                repeat: false,
                volume: 1.0,
                ..default()
            },
        );
    }
    if attempting_to_jump && player_vel.linvel.y < 0.0 && feather_fall_power {
        player_grav.0 = GRAVITY_SCALE / 5.0;
    } else {
        player_grav.0 = GRAVITY_SCALE;
    }

    // Handle rolling away from/towards the camera.
    if keys.any_just_pressed([KeyCode::Up, KeyCode::Down]) {
        if !rapier_context
            .intersections_with(player)
            .any(|(_, _, intersecting)| intersecting)
        {
            // The rolling away/towards is successful, so we propagate a map
            // state change.
            let new_map_state = match *map_state {
                MapHalf::Fore => {
                    *map_state = MapHalf::Back;

                    MapHalf::Back
                }
                MapHalf::Back => {
                    *map_state = MapHalf::Fore;

                    MapHalf::Fore
                }
            };

            // We also have to decide which face we are rolling to.
            let old_face = *player_face;
            let possible_faces = old_face.adjacent_faces();
            let theta_raw = player_tform.rotation.to_euler(EulerRot::ZYX).0;
            let theta = 2.0 * PI - theta_raw;
            let new_face_ix = (theta * (2.0 / PI) + 0.5) as usize;
            let backwards = new_map_state == MapHalf::Back;
            let new_face = possible_faces[if backwards {
                3 - new_face_ix % 4
            } else {
                new_face_ix % 4
            }];
            *player_face = new_face;

            let theta_raw_adjusted = theta_raw + PI / 4.0;
            player_asr.0 =
                theta_raw_adjusted - theta_raw_adjusted.rem_euclid(PI / 2.0);

            // Dispatch the map state change event.
            msc_er.send(MapStateChange {
                new_half: new_map_state,
                old_face,
                new_face,
            });

            // Play cute sound courtesy of Tab.
            audio.play_with_settings(
                sfx.switch.clone(),
                PlaybackSettings {
                    repeat: false,
                    volume: 1.0,
                    ..default()
                },
            );
        }
    }
}

fn process_collisions(
    rapier_context: Res<RapierContext>,
    mut ptf_res: ResMut<PlayerTouchingFloor>,
    coll_q: Query<(Entity, &Transform, Option<&Player>), With<Collider>>,
    vel_q: Query<&Velocity>,
    audio: Res<Audio>,
    sfx: Res<Sfx>,
) {
    let mut ptf = PlayerTouchingFloor::No;
    let mut played_hit_sound = false;

    // Query Rapier for any contact pairs that were just generated during this
    // frame.
    for contact_pair in rapier_context.contact_pairs() {
        // If this contact pair was determined not to be an active contact
        // during the narrow-phase, we ignore it, as it does not actually
        // represent a contact at all.
        if !contact_pair.has_any_active_contacts() {
            continue;
        }

        let (collider1_pos, collider1_is_player) = coll_q
            .get(contact_pair.collider1())
            .map(|(_, tform, player)| {
                (tform.translation.truncate(), player.is_some())
            })
            .unwrap();
        let (collider2_pos, collider2_is_player) = coll_q
            .get(contact_pair.collider2())
            .map(|(_, tform, player)| {
                (tform.translation.truncate(), player.is_some())
            })
            .unwrap();

        // Observe just one manifold (there probably is only one anyways, and
        // it seems that one is all that we need) associated with this contact
        // pair.
        for manifold in contact_pair.manifolds() {
            // The normal is already expressed in world-space.
            let manifold_normal = manifold.normal();

            if collider1_is_player || collider2_is_player {
                let downward_magnitude =
                    (manifold_normal.y / manifold_normal.length()).abs();

                let (player_pos, player_vel, ground_pos) =
                    if collider1_is_player {
                        (
                            collider1_pos,
                            vel_q.get(contact_pair.collider1()).unwrap(),
                            collider2_pos,
                        )
                    } else {
                        (
                            collider2_pos,
                            vel_q.get(contact_pair.collider2()).unwrap(),
                            collider1_pos,
                        )
                    };

                if !played_hit_sound {
                    let player_speed = player_vel.linvel.length();

                    if player_speed >= 1.0 {
                        played_hit_sound = true;

                        let speed_normalised = player_speed.min(24.0) / 24.0;
                        audio.play_with_settings(
                            sfx.hit.clone(),
                            PlaybackSettings {
                                repeat: false,
                                volume: speed_normalised * speed_normalised,
                                ..default()
                            },
                        );
                    }
                }

                if downward_magnitude > 0.75 && player_pos.y > ground_pos.y {
                    ptf = PlayerTouchingFloor::Yes;

                    break;
                }
            }
        }

        if ptf == PlayerTouchingFloor::Yes {
            break;
        }
    }

    *ptf_res = match (*ptf_res, ptf) {
        (PlayerTouchingFloor::CanDoubleJump, PlayerTouchingFloor::Yes) => {
            PlayerTouchingFloor::Yes
        }
        (PlayerTouchingFloor::CanDoubleJump, _) => {
            PlayerTouchingFloor::CanDoubleJump
        }
        _ => ptf,
    };
}

fn move_camera(
    time: Res<Time>,
    die_q: Query<Entity, With<Player>>,
    cam_q: Query<Entity, With<Camera>>,
    mut tform_q: Query<
        (&mut Transform, Option<&Backdrop>),
        Or<(With<Player>, With<Camera>, With<Backdrop>)>,
    >,
    map: Res<Map>,
) {
    let (die, cam) = (die_q.single(), cam_q.single());
    let die_pos = tform_q.get(die).unwrap().0.translation.truncate();
    let cam_pos = tform_q.get(cam).unwrap().0.translation.truncate();

    let disp = die_pos - cam_pos;
    let disp_step = disp.length() * time.delta_seconds() * disp.normalize();

    let mut cam_tform = tform_q.get_mut(cam).unwrap();
    cam_tform.0.translation += disp_step.extend(0.0);
    let new_cam_pos = cam_tform.0.translation;

    // Handle the backdrop manipulation.
    let map_width = map.width().get() as f32;
    let map_height = (map.height() + 3) as f32;
    for (mut tform, bd) in tform_q.iter_mut() {
        if bd.is_none() {
            continue;
        }

        let x_prop = (new_cam_pos.x / map_width).clamp(0.0, 1.0) - 0.5;
        let y_prop = (new_cam_pos.y / map_height).clamp(0.0, 1.0) - 0.5;

        // We multiply by the bespoke Z values to induce a parallax effect.

        // 5.0 = (BG_TEXTURE_WIDTH * (PROJECTION_SCALE * BACKDROP_SCALE) -
        // WINDOW_WIDTH) / PROJECTION_SCALE
        tform.translation.x =
            new_cam_pos.x - 5.0 * x_prop * tform.translation.z;
        // 3.0 = (BG_TEXTURE_HEIGHT * (PROJECTION_SCALE * BACKDROP_SCALE) -
        // WINDOW_HEIGHT) / PROJECTION_SCALE
        tform.translation.y =
            new_cam_pos.y - 3.0 * y_prop * tform.translation.z;
    }
}
