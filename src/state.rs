use bevy::ecs::component::Component;

#[derive(Component, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum MapHalf {
    Fore,
    Back,
}
