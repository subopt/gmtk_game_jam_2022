use anyhow::Error;
use bevy::math::Vec2;
use bevy_rapier2d::geometry::Collider;
use num_enum::{IntoPrimitive, TryFromPrimitive};
use serde::Deserialize;
use std::{
    num::NonZeroUsize,
    ops::{Index, IndexMut},
};

#[derive(Deserialize)]
pub struct Map {
    fg: Ground,
    bg: Ground,
    spawn_point: Vec2,
}

#[derive(Deserialize)]
pub struct Ground {
    shapes: Vec<GroundShape>,
    width: NonZeroUsize,
}

pub struct GroundIterCoords<'iter> {
    ground: &'iter Ground,
    x: usize,
    y: usize,
}

#[derive(
    Clone,
    Copy,
    PartialEq,
    Eq,
    Debug,
    IntoPrimitive,
    TryFromPrimitive,
    Deserialize,
)]
#[repr(u8)]
pub enum GroundShape {
    // Full squares.
    FullR = 0,
    FullL = 1,
    // Main diagonals.
    MdR = 2,
    MdL = 3,
    MdRp = 4,
    MdLp = 5,
    // Half-diagonals.
    HdR = 6,
    HdL = 7,
    HdRp = 8,
    HdLp = 9,
    HdRpp = 10,
    HdLpp = 11,
    HdRppp = 12,
    HdLppp = 13,
    // Half squares (2:1).
    HalfR = 14,
    HalfL = 15,
    HalfRp = 16,
    HalfLp = 17,
}

impl Map {
    pub fn new(
        width: NonZeroUsize,
        foregrid: Vec<GroundShape>,
        backgrid: Vec<GroundShape>,
        spawn_point: Vec2,
    ) -> Self {
        Self {
            fg: Ground {
                shapes: foregrid,
                width,
            },
            bg: Ground {
                shapes: backgrid,
                width,
            },
            spawn_point,
        }
    }

    pub fn from_json(json: &str) -> Result<Self, Error> {
        Ok(serde_json::from_str(json)?)
    }

    pub fn foreground(&self) -> &Ground {
        &self.fg
    }

    pub fn background(&self) -> &Ground {
        &self.bg
    }

    pub fn spawn_point(&self) -> Vec2 {
        self.spawn_point
    }

    pub fn width(&self) -> NonZeroUsize {
        self.fg.width().max(self.bg.width())
    }

    pub fn height(&self) -> usize {
        self.fg.height().max(self.bg.height())
    }
}

impl Ground {
    /// Returns the grid element at the specified coordinate, if any.
    ///
    /// Grid elements are stored from left to right, and then bottom to top. In
    /// effect, we view the grid as occupying the corner of the first
    /// [quadrant] (quadrant I) of a 2D Cartesian coordinate system. Thus, `x`
    /// values, and `y` values, are always nonnegative.
    ///
    /// To give a concrete example, consider a `Ground` that has a height of 3
    /// and a width of 4 (`self.width == 4`). Using hexadecimal numerals for
    /// readability, we can represent the raw indices (the indices into the
    /// underlying `Vec`, each of which is a single `usize`) graphically, like
    /// so:
    ///
    /// ```text
    /// 8 9 A B
    /// 4 5 6 7
    /// 0 1 2 3
    /// ```
    ///
    /// The corresponding indices according to this trait implementation (where
    /// each index is an ordered pair of natural numbers, viz.
    /// `(usize, usize)`) are then:
    ///
    /// ```text
    /// (0,2) (1,2) (2,2) (3,2)
    /// (0,1) (1,1) (2,1) (3,1)
    /// (0,0) (1,0) (2,0) (3,0)
    /// ```
    ///
    /// …Where each tuple is of the form (`x`,`y`).
    ///
    /// [quadrant]: https://en.wikipedia.org/wiki/Quadrant_(plane_geometry)
    pub fn get(&self, x: usize, y: usize) -> Option<GroundShape> {
        self.shapes.get(x + y * self.width.get()).copied()
    }

    /// Returns an iterator over the grid elements (`GroundShape`s) of this
    /// `Ground`, with each element accompanied by its coordinate.
    pub fn iter_coords(&self) -> GroundIterCoords {
        GroundIterCoords {
            ground: self,
            x: 0,
            y: 0,
        }
    }

    pub fn width(&self) -> NonZeroUsize {
        self.width
    }

    pub fn height(&self) -> usize {
        /// Nabbed from
        /// <https://github.com/rust-lang/rust/blob/f1a8854f9be2e5cad764d630a53d26c7b72f8162/library/core/src/num/uint_macros.rs#L2042>
        #[inline]
        fn div_ceil(lhs: usize, rhs: NonZeroUsize) -> usize {
            let d = lhs / rhs;
            let r = lhs % rhs;
            if r > 0 {
                d + 1
            } else {
                d
            }
        }

        div_ceil(self.shapes.len(), self.width)
    }
}

impl Index<(usize, usize)> for Ground {
    type Output = GroundShape;

    /// See the documentation for `Ground::get`.
    fn index(&self, (x, y): (usize, usize)) -> &Self::Output {
        &self.shapes[x + y * self.width.get()]
    }
}

impl IndexMut<(usize, usize)> for Ground {
    /// See the documentation for `Ground::get`.
    fn index_mut(&mut self, (x, y): (usize, usize)) -> &mut Self::Output {
        &mut self.shapes[x + y * self.width.get()]
    }
}

impl<'iter> Iterator for GroundIterCoords<'iter> {
    type Item = (GroundShape, usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        let maybe_gs = self.ground.get(self.x, self.y);
        let (x, y) = (self.x, self.y);

        self.x += 1;
        if self.x >= self.ground.width.get() {
            self.x = 0;
            self.y += 1;
        }

        maybe_gs.map(|gs| (gs, x, y))
    }
}

impl GroundShape {
    pub fn collider(self) -> Option<Collider> {
        match self {
            // Full squares.
            Self::FullR => Some(Collider::cuboid(0.5, 0.5)),
            Self::FullL => None,

            // Main diagonals.
            Self::MdR => Some(Collider::triangle(
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            )),
            Self::MdL => Some(Collider::triangle(
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            )),
            Self::MdRp => Some(Collider::triangle(
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(0.5, -0.5),
            )),
            Self::MdLp => Some(Collider::triangle(
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, -0.5),
            )),

            // Half-diagonals.
            Self::HdR => Some(Collider::triangle(
                Vec2::new(0.5, 0.0),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            )),
            Self::HdL => Some(Collider::triangle(
                Vec2::new(-0.5, 0.0),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            )),
            Self::HdRp => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.0),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            ]),
            Self::HdLp => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.0),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            ]),
            Self::HdRpp => Some(Collider::triangle(
                Vec2::new(0.5, 0.0),
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
            )),
            Self::HdLpp => Some(Collider::triangle(
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, 0.0),
            )),
            Self::HdRppp => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, 0.0),
                Vec2::new(0.5, -0.5),
            ]),
            Self::HdLppp => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.0),
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, -0.5),
            ]),

            // Half squares (2:1).
            Self::HalfR => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.0),
                Vec2::new(-0.5, 0.0),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.5, -0.5),
            ]),
            Self::HalfL => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.0),
                Vec2::new(0.5, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, 0.0),
            ]),
            Self::HalfRp => Collider::convex_polyline(vec![
                Vec2::new(0.5, 0.5),
                Vec2::new(0.0, 0.5),
                Vec2::new(0.0, -0.5),
                Vec2::new(0.5, -0.5),
            ]),
            Self::HalfLp => Collider::convex_polyline(vec![
                Vec2::new(0.0, 0.5),
                Vec2::new(-0.5, 0.5),
                Vec2::new(-0.5, -0.5),
                Vec2::new(0.0, -0.5),
            ]),
        }
    }
}
